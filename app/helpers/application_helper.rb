module ApplicationHelper

  def sales_order_admin_status sold_to_party, sales_order_id, status
    case status
      when 'Pending for Release','Approved','Credit Hold'
        "A new sales order for <b>#{sold_to_party}</b> with reference number <b>#{sales_order_id}</b> is #{status == 'Credit Hold' ? "on <b>#{status}</b>" : "<b>#{status}</b>" }#{' and requires your attention' if status != 'Credit Hold'}."
      when 'In-Production', 'Awaiting Ref No', 'Awaiting Customer Collection','Received by customer'
        "Sales order for <b>#{sold_to_party}</b> with reference number <b>#{sales_order_id}</b> #{status == 'Received by customer' ? "was <b>#{status}</b>" : "is <b>#{status}</b>" }#{' and requires your attention' if status == 'In-Production'}."
    end
  end

  def sales_order_customer_status sold_to_party, sales_order_id, status
    case status
      when 'Pending for Release','In-Production', 'Awaiting Ref No', 'Awaiting Customer Collection','Credit Hold'
        "#{ status == 'Pending for Release' ? 'A new sales order' : 'Sales order' } for <b>#{sold_to_party}</b> with reference number <b>#{sales_order_id}</b> is #{status == 'Credit Hold' ? "on <b>#{status}</b>" : "<b>#{status}</b>" }."
      when 'Approved','Received by customer'
        "Sales order for <b>#{sold_to_party}</b> with reference number <b>#{sales_order_id}</b> was <b>#{status}</b>."
    end
  end
end
