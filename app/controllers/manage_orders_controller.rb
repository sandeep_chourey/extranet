class ManageOrdersController < InheritedResources::Base
  def index
    @list_of_ship_to_party = @current_customer.list_of_ship_to_party
    @list_of_sold_to_party = @current_customer.list_of_sold_to_party
  end

  def create
    sales_order = SalesOrder.create(sales_order_params)
    params[:material_code].each_with_index do |material_code,index|
      MaterialCode.create(:sales_order_id => sales_order.id, :material_code => material_code,:order_quantity => params[:order_quantity][index], :unit_of_measure => params[:unit_of_measure][index], :price => params[:price][index], :currency => params[:currency][index])
    end
    # params[:ship_to_party].each_with_index do |ship_to,index|
      # OrderDeliveryParty.create(:sales_order_id => sales_order.id, :sold_to => sales_order.sold_to_party,:ship_to => ship_to)
    # end
    SalesOrderMailer.sales_order_notification(sales_order).deliver!
    flash[:notice] = "Thank You for placing your order. Shortly after your order is approved, one of our customer service representatives will e-mail you an order confirmation. Have a nice day."
    redirect_to root_url
  end

  def list_of_orders
    if current_customer
      if current_customer.is_admin
        @sales_orders =  SalesOrder.joins(:customer).where("customers.type = ? ", current_customer.type)
      else
        @sales_orders =  SalesOrder.joins(:customer).where("customers.id = ? OR sales_orders.ship_to_party = ? OR sales_orders.sold_to_party = ? ", current_customer.id, current_customer.ship_to, current_customer.sold_to)
      end
    elsif current_assistant
      @sales_orders =  SalesOrder.joins(:customer).where("customers.type = ? ", current_assistant.type == 'EasternAssistant' ? 'Eastern' : 'Western' )
    end

    if current_customer && current_customer.is_admin
      title = current_customer.type
    elsif current_assistant && current_assistant.type
      title = (current_assistant.type == 'EasternAssistant' ? 'Eastern' : 'Western')
    end

    @listing_title = ( title ) ? "Sales Order Listings of #{title} Contacts" : "Sales Order Listings"
  end

  def update_order_status
    @sales_order = SalesOrder.find(params[:id])
    @sales_order.update_attributes(:status => params[:status])
    #if @sales_order.status == "Approved"
      SalesOrderMailer.sales_order_approved_notification(@sales_order).deliver!
      SalesOrderMailer.sales_order_user_notification(@sales_order).deliver!
    #end
    render :json => { :message => "Sales Order status updated successfully" }
  end

  def show
    @sales_order = SalesOrder.find(params[:id])
  end

  def party_name
    code = params[:code]
    party_name = ''

    if params[:type] == 'ship_to'
      party_name = Customer.ship_to_party_name(code)
    else
      party_name = Customer.sold_to_party_name(code)
    end
    ship_to_parties = SoldToShipmentParty.where(:sold_to => code ).order('ship_to asc').pluck(:ship_to)
    render  :json => { :party_name => party_name, :ship_to_parties => ship_to_parties }
  end

  def show_material_description
    material_detail = MaterialDetail.find_by_material_code(params[:code])
    customers_material_detail = CustomersMaterialDetail.find_by_material_detail_id_and_customer_id(material_detail.id,@current_customer.id)
    render :json => { :material_description => material_detail.material_description, :currency => customers_material_detail.currency, price: customers_material_detail.price, unit_of_measure: customers_material_detail.unit_of_measure  }
  end

  private

  def sales_order_params
    params.require(:sales_order).permit(:sold_to_party, :ship_to_party, :required_delivery_date, :material_code, :order_quantity, :unit_of_measure, :po_number, :po_date, :text,:status, :customer_id)
  end
end