ActiveAdmin.register SalesOrder do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs do
      f.input :customer_id, :as => :select, :collection => Customer.all.order("first_name").map{ |s| [s.first_name+' '+s.last_name, s.id] }
      f.input :sold_to_party
      f.input :ship_to_party
      f.input :required_delivery_date
      f.input :incoterms
      f.input :po_number
      f.input :po_date
      f.input :text
      f.input :status
    end
  end

  filter :customer_id, as: :select, collection: Customer.all.order("first_name").map{ |s| [s.first_name+' '+s.last_name, s.id] }
  filter :material_codes, as: :select, collection: MaterialCode.all.order("material_code").map{ |md| [md.material_code, md.id] }
  # filter :order_delivery_parties, as: :select, collection: OrderDeliveryParty.all.map{ |odp| [odp.sales_order_id.to_s + ' - ' + odp.sold_to, odp.id] }
  filter :ship_to_party
  filter :sold_to_party
  filter :required_delivery_date
  filter :incoterms
  filter :po_number
  filter :po_date
  filter :text
  filter :status
  filter :created_at
  filter :updated_at
end
