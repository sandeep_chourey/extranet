ActiveAdmin.register ActivityType do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  filter :admin_notification_configs, as: :select, collection: AdminNotificationConfig.all.map{ |s| [s.id, s.id] }
  filter :name
  filter :created_at
  filter :updated_at
end
