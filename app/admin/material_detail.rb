ActiveAdmin.register MaterialDetail do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  filter :customers, as: :select, collection: Customer.all.order("first_name").map{ |s| [s.first_name+' '+s.last_name, s.id] }
  filter :customers_material_details, as: :select, collection: CustomersMaterialDetail.all.map{ |s| [s.id, s.id] }
  filter :material_code
  filter :material_description
  filter :created_at
  filter :updated_at

end
