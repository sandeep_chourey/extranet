ActiveAdmin.register Assistant do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  permit_params :email, :password, :password_confirmation, :first_name, :last_name, :company_name, :company_address, :company_zip, :company_website, :company_country, :type

  controller do
    def update
      if params[:assistant][:password].blank? && params[:assistant][:password_confirmation].blank?
        params[:assistant].delete("password")
        params[:assistant].delete("password_confirmation")
      end
      super
    end
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Assistant Profile" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :first_name
      f.input :last_name
      f.input :type, :as => :select, :collection => [['Eastern Assistant','EasternAssistant'],['Western Assistant','WesternAssistant']]
    end

    f.inputs "Assistant company detail" do
      f.input :company_name
      f.input :company_address
      f.input :company_country, as: :string, collection: ActionView::Helpers::FormOptionsHelper::COUNTRIES
      f.input :company_zip
      f.input :company_website
    end
    f.actions
  end
  
end
