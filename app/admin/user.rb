ActiveAdmin.register User do

        form do |f|
            f.inputs "User Details" do
                f.input :email
                f.input :password
                f.input :password_confirmation
           end
           f.inputs "Product Ranges" do
              f.input :product_ranges, :as => :check_boxes
           end
           f.inputs "Roles" do
              f.input :spree_roles, :as => :check_boxes
           end
           f.actions
        end
        
        index do
           column :first_name
           column :last_name
           column :email
           column :last_sign_in_at
           default_actions
        end
                
end
