ActiveAdmin.register CustomersMaterialDetail do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs "My Model Name" do
      # add your other inputs
      f.input :material_detail, :collection => MaterialDetail.all.map{ |md| [md.material_code, md.id] }
      f.input :customer, :collection => Customer.all.map{ |c| [c.first_name+' '+ c.last_name + ': '+ c.id.to_s, c.id] }
      f.input :currency
      f.input :price
      f.input :unit_of_measure

      f.actions
    end
  end

  index do
    selectable_column
    column :id
    column "Customer" do |c|
      customer = Customer.find(c.customer_id)
      customer.first_name + ' ' + customer.last_name
    end
    column "Material Code" do |md|
      material_detail = MaterialDetail.find(md.material_detail_id)
      material_detail.material_code
    end
    column :currency
    column :price
    column :unit_of_measure
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    # panel "Id" do
    #   customer_material_detail.id
    # end
    # panel "Customer" do
    #   customer = Customer.find(customer_material_detail.customer_id)
    #   customer..first_name + ' ' + customer.last_name
    # end
    #
    # panel "Material Code" do
    #   MaterialDetail.find(customer_material_detail.material_detail_id).material_code
    # end
    attributes_table do
      row :id
      row 'CUSTOMER' do |c|
        customer = Customer.find(c.customer_id)
        customer.first_name + ' ' + customer.last_name
      end
      row "Material Code" do |md|
        material_detail = MaterialDetail.find(md.material_detail_id)
        material_detail.material_code
      end
      row :currency
      row :price
      row :unit_of_measure
      row :created_at
      row :updated_at
    end

  end

  filter :customer_id, as: :select, collection: Customer.all.order("first_name").map{ |s| [s.first_name+' '+s.last_name, s.id] }
  filter :material_detail, as: :select, collection: MaterialDetail.all.map{ |s| [s.id, s.id] }
  filter :unit_of_measure
  filter :price
  filter :currency
  filter :created_at
  filter :updated_at
end
