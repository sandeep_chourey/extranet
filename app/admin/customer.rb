ActiveAdmin.register Customer do

  # menu :priority => 4, :label => "Customer"
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :first_name, :last_name, :email
  permit_params :email, :password, :password_confirmation, :first_name, :last_name, :company_name, :company_address, :company_zip, :company_website, :company_country,:is_admin, :type,:sold_to,:ship_to
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  controller do
    def update
      if params[:customer][:password].blank? && params[:customer][:password_confirmation].blank?
        params[:customer].delete("password")
        params[:customer].delete("password_confirmation")
      end
      super
    end
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Customer Profile" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :first_name
      f.input :last_name
      f.input :is_admin
      f.input :type, :as => :select, :collection => [['Eastern','Eastern'],['Western','Western']]
      f.input :sold_to
      f.input :ship_to
    end

    f.inputs "Customer company detail" do
      f.input :company_name
      f.input :company_address
      f.input :company_country, as: :string, collection: ActionView::Helpers::FormOptionsHelper::COUNTRIES
      f.input :company_zip
      f.input :company_website
    end
    f.actions
  end

  filter :customers_material_details, as: :select, collection: CustomersMaterialDetail.all.map{ |s| [s.id, s.id] }
  filter :material_details, as: :select, collection: MaterialDetail.all.map{ |s| [s.id, s.id] }
  filter :sales_orders, as: :select, collection: SalesOrder.all.map{ |s| [s.id, s.id] }
  filter :customer_sold_to_parties, as: :select, collection: CustomerSoldToParty.all.map{ |s| [s.id, s.id] }

  filter :email
  filter :first_name
  filter :last_name
  filter :is_admin
  filter :sold_to
  filter :ship_to
  filter :company_name
  filter :company_address
  filter :company_country
  filter :company_zip
  filter :company_website
  filter :created_at
  filter :updated_at
end
