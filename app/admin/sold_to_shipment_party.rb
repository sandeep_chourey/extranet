ActiveAdmin.register SoldToShipmentParty do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs "Sold To Shipment Party" do
      # add your other inputs
      f.input :sold_to, :collection => Customer.all.map{ |s| [s.sold_to, s.sold_to] }
      f.input :ship_to, :collection => Customer.all.map{ |s| [s.ship_to, s.ship_to] }

      f.actions
    end
  end

  index do
    selectable_column
    column :id
    column :sold_to
    column :ship_to
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    # panel "Id" do
    #   customer_material_detail.id
    # end
    # panel "Customer" do
    #   customer = Customer.find(customer_material_detail.customer_id)
    #   customer..first_name + ' ' + customer.last_name
    # end
    #
    # panel "Material Code" do
    #   MaterialDetail.find(customer_material_detail.material_detail_id).material_code
    # end
    attributes_table do
      row :id
      row :sold_to
      row :ship_to
      row :created_at
      row :updated_at
    end

  end


end
