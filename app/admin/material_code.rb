ActiveAdmin.register MaterialCode do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  action_item :only => [:show, :edit] do
    link_to 'Create New Material Code', '/admin/material_codes/new'
  end

  filter :sales_order, as: :select, collection: SalesOrder.all.map{ |s| [s.id, s.id] }
  filter :material_code
  filter :order_quantity
  filter :unit_of_measure
  filter :price
  filter :currency
  filter :created_at
  filter :updated_at

end
