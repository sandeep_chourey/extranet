ActiveAdmin.register AdminNotificationConfig do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs "Admin Notification Configuration" do
      # add your other inputs
      f.input :activity_type_id, as: :select, :collection => ActivityType.all.map{ |at| [at.name + ': '+ at.id.to_s, at.id] }, include_blank: false
      f.input :admin_region, :collection => [['Eastern','Eastern'],['Western','Western'],['All','All']], include_blank: false
      f.input :notification_emails

      f.actions
    end
  end

  index do
    selectable_column
    column :id
    column "Activity Type" do |c|
      c.activity_type.name
    end
    column :admin_region
    column :notification_emails
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    # panel "Id" do
    #   customer_material_detail.id
    # end
    # panel "Customer" do
    #   customer = Customer.find(customer_material_detail.customer_id)
    #   customer..first_name + ' ' + customer.last_name
    # end
    #
    # panel "Material Code" do
    #   MaterialDetail.find(customer_material_detail.material_detail_id).material_code
    # end
    attributes_table do
      row :id
      row 'Activity Type' do |c|
        c.activity_type.name
      end
      row :admin_region
      row :notification_emails
      row :created_at
      row :updated_at
    end
  end

end
