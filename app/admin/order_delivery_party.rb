ActiveAdmin.register OrderDeliveryParty do

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end


  form do |f|
    f.inputs "My Model Name" do
      # add your other inputs
      f.input :sales_order_id
      f.input :ship_to
      f.input :sold_to

      f.actions
    end
  end

  index do
    selectable_column
    column :id
    column :sales_order_id
    column :ship_to
    column :sold_to
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    # panel "Id" do
    #   customer_material_detail.id
    # end
    # panel "Customer" do
    #   customer = Customer.find(customer_material_detail.customer_id)
    #   customer..first_name + ' ' + customer.last_name
    # end
    #
    # panel "Material Code" do
    #   MaterialDetail.find(customer_material_detail.material_detail_id).material_code
    # end
    attributes_table do
      row :id
      row :sales_order_id
      row :ship_to
      row :sold_to
      row :created_at
      row :updated_at
    end

  end
end
