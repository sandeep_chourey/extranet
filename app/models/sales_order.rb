class SalesOrder < ActiveRecord::Base
  def self.table_name_prefix
    self.connection.current_database+'.'
  end
  establish_connection "development_sec"
  # validates :po_date, presence: true, format: "%Y-%m-%d"
  has_many :material_codes
  has_many :order_delivery_parties
  belongs_to :customer

  def total_order_detail
    total_order_price = 0
    total_order_quantity = 0
    currency = ''
    self.material_codes.each do |mc|
      material_detail = MaterialDetail.find_by_material_code(mc.material_code)
      #customers_material_detail = CustomersMaterialDetail.find_by_material_detail_id_and_customer_id(material_detail.id,self.customer_id)
      quantity = mc.order_quantity
      currency = mc.currency
      total_order_price += (mc.price * quantity)
      total_order_quantity += quantity
    end

    return total_order_price, total_order_quantity, currency
  end


end
