class MaterialCode < ActiveRecord::Base
  establish_connection "development_sec"
  belongs_to :sales_order

  def material_description
    if self.material_code.present?
      material_detail = MaterialDetail.find_by_material_code(self.material_code)
      material_detail.material_description if material_detail.present?
    end
  end

  def order_net_value

    if self.material_code.present?
      material_detail = MaterialDetail.find_by_material_code(self.material_code)
      customers_material_detail = CustomersMaterialDetail.find_by_material_detail_id_and_customer_id(material_detail.id,self.sales_order.customer_id)
      quantity = self.order_quantity
      currency = customers_material_detail.currency
      order_net_value = (customers_material_detail.price * quantity)
    end
    return customers_material_detail.price, currency, order_net_value
  end

  def total_order_detail
    total_order_price = 0
    total_order_quantity = 0
    self.material_codes.each do |mc|
      material_detail = MaterialDetail.find_by_material_code(mc.material_code)
      quantity = mc.order_quantity
      total_order_price += (material_detail.price * quantity)
      total_order_quantity += quantity
    end

    return total_order_price, total_order_quantity
  end
end
