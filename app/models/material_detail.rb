class MaterialDetail < ActiveRecord::Base
  establish_connection "development_sec"
  has_many :customers, through: :customers_material_details
  has_many :customers_material_details
end
