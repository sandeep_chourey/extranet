class CustomersMaterialDetail < ActiveRecord::Base
  establish_connection "development_sec"
  belongs_to :customer
  belongs_to :material_detail
end
