class AdminAbility
  include CanCan::Ability

  def initialize(user)
    if Role.find(:all, :include=>:admin_users, :conditions => ["admin_users.id = ? AND name = ?", user.id, "Root"]).count > 0
       can :read, Category
       can :manage, Category
       can :read, TeamMember
       can :manage, TeamMember
       can :read, Role
       can :manage, Role
       can :read, User
       can :manage, User
       can :read, AdminUser
       can :manage, AdminUser
       can :read, Promotion
       can :manage, Promotion
       can :read, PromotionCategory
       can :manage, PromotionCategory
       can :read, PromotionPaymentStatus
       can :manage, PromotionPaymentStatus
       can :read, ActiveAdmin::Page, :name => "Store"
    end
    can :read, Attachment
    can :manage, Attachment
    can :read, Gallery
    can :manage, Gallery
    can :read, GalleryImage
    can :manage, GalleryImage
    can :read, Event
    can :manage, Event
    can :read, Post
    can :manage, Post
    can :read, Customer
    can :manage, Customer
    can :read, MaterialDetail
    can :manage, MaterialDetail
    can :read, CustomersMaterialDetail
    can :manage, CustomersMaterialDetail
    #can :read, OrderDeliveryParty
    #can :manage, OrderDeliveryParty
    can :read, Assistant
    can :manage, Assistant
    can :read, SoldToShipmentParty
    can :manage, SoldToShipmentParty
    can :read, CustomerSoldToParty
    can :manage, CustomerSoldToParty
    can :read, SalesOrder
    can :manage, SalesOrder
    can :read, MaterialCode
    can :manage, MaterialCode
    can :read, AdminNotificationConfig
    can :manage, AdminNotificationConfig
    can :read, ActivityType
    can :manage, ActivityType
    can :read, Attachment
    can :manage, Attachment    
    can :read, AdminUser, :id => user.id
    can :manage, AdminUser, :id => user.id
    can :read, ActiveAdmin::Page, :name => "Dashboard"
  end

end
