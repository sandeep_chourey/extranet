class AdminNotificationConfig < ActiveRecord::Base
  def self.table_name_prefix
    self.connection.current_database+'.'
  end
  establish_connection 'development_sec'
  belongs_to :activity_type

  scope :admin_config_with_region, -> (activity_id,admin_region) { where(activity_type_id: activity_id, admin_region: admin_region) }
end
