class ActivityType < ActiveRecord::Base
  def self.table_name_prefix
    self.connection.current_database+'.'
  end
  establish_connection 'development_sec'
  has_many :admin_notification_configs
end
