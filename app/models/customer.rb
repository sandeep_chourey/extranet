class Customer < ActiveRecord::Base

  def self.table_name_prefix
    self.connection.current_database+'.'
  end

  establish_connection "development"

  has_many :material_details, through: :customers_material_details
  has_many :customers_material_details
  has_many :customer_sold_to_parties

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :registerable,
        :authentication_keys => [:email]
  validates :password, :password_confirmation, presence: true, on: :create


  # attr_accessible :email, :encrypted_password, :password, :password_confirmation
  has_many :sales_orders

  def list_of_ship_to_party
    list_of_customer = Customer.where(type: self.type).order('ship_to asc')
    list_of_customer.map{|c| [c.ship_to,c.ship_to]  if !c.ship_to.blank?  }.compact if !list_of_customer.blank?
  end

  def list_of_sold_to_party
    # list_of_customer = Customer.where(type: self.type)
    list_of_customer = self.customer_sold_to_parties.order('sold_to asc')
    list_of_customer.map{|c| [c.sold_to,c.sold_to] if !c.sold_to.blank? }.compact if !list_of_customer.blank?
  end

  def self.ship_to_party_name(code)
    customer = Customer.find_by_ship_to(code)
    # "#{customer.first_name} #{customer.last_name}" if !customer.blank?
    customer.company_name if !customer.blank?
  end

  def ship_to_company_name(code)
    customer = Customer.find_by_ship_to(code)
    # "#{customer.first_name} #{customer.last_name}" if !customer.blank?
    customer.company_name if !customer.blank?
  end

  def self.sold_to_party_name(code)
    customer = Customer.find_by_sold_to(code)
    # "#{customer.first_name} #{customer.last_name}" if !customer.blank?
    customer.company_name if !customer.blank?
  end

  def   list_of_material_code
    self.material_details.sort_by { |md| md.material_code }.map{|md| [md.material_code,md.material_code]}
  end
end
