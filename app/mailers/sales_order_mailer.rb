class SalesOrderMailer < ActionMailer::Base
   default from: "xnet.angostura@gmail.com"
   helper ApplicationHelper
  def sales_order_notification(sales_order)
    @sales_order = sales_order
    @activity = ActivityType.find_by_name('Sales Order Creation')
    @admin_notification_config = AdminNotificationConfig.admin_config_with_region(@activity.id, sales_order.customer.type).first
    mail(:to => @admin_notification_config.notification_emails || 'andyholmes@artisan-spirits.co.uk; abaksh@angostura.com;', :subject => "New Sales Order")
  end

  def sales_order_approved_notification(sales_order)
    @sales_order = sales_order
    @activity = ActivityType.find_by_name(@sales_order.status)
    @admin_notification_config = AdminNotificationConfig.admin_config_with_region(@activity.id, sales_order.customer.type).first
    mail(:to => @admin_notification_config.notification_emails || 'samo@angostura.com; mcdonam@angostura.com; RicharG@angostura.com; abaksh@angostura.com', :subject => "Sales Order #{@sales_order.status}")
  end

   def sales_order_user_notification(sales_order)
     @sales_order = sales_order
     @customer = Customer.find_by_sold_to(@sales_order.sold_to_party)
     mail(:to => @customer.email, :subject => "Sales Order #{@sales_order.status}")
   end
end
