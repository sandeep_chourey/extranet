class CreateMaterialDetails < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    create_table :material_details do |t|
      t.string :material_code
      t.string :material_description

      t.timestamps
    end
    ActiveRecord::Base.establish_connection "development"
  end
end
