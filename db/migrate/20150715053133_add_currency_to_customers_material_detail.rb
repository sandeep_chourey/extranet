class AddCurrencyToCustomersMaterialDetail < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"
  def change
    add_column :customers_material_details, :currency, :string, default: '$'
    add_column :customers_material_details, :price, :integer, default: '1'
    ActiveRecord::Base.establish_connection "development"
  end
end
