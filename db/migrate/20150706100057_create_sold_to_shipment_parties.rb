class CreateSoldToShipmentParties < ActiveRecord::Migration
  def change
    create_table :sold_to_shipment_parties do |t|
      t.string :sold_to
      t.string :ship_to

      t.timestamps
    end
  end
end
