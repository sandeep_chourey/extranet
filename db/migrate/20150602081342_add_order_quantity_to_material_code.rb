class AddOrderQuantityToMaterialCode < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    add_column :material_codes, :order_quantity, :integer
    add_column :material_codes, :unit_of_measure, :string
    ActiveRecord::Base.establish_connection "production"
  end
end
