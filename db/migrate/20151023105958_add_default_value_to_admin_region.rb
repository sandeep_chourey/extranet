class AddDefaultValueToAdminRegion < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def up
    change_column :admin_notification_configs, :admin_region, :string, :default => 'All'
    ActiveRecord::Base.establish_connection "development"
  end

  def down
    change_column :admin_notification_configs, :admin_region, :string, :default => nil
    ActiveRecord::Base.establish_connection "development"
  end
end
