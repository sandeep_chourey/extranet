class CreateActivityTypes < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    create_table :activity_types do |t|
      t.string :name

      t.timestamps
    end
    ActiveRecord::Base.establish_connection "development"
  end
end
