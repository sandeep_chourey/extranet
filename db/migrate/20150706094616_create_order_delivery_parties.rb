class CreateOrderDeliveryParties < ActiveRecord::Migration
  def change
    create_table :order_delivery_parties do |t|
      t.integer :sales_order_id
      t.string :sold_to
      t.string :ship_to

      t.timestamps
    end
  end
end
