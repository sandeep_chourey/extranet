class AddShipToToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :ship_to, :string
    add_column :customers, :sold_to, :string
  end
end
