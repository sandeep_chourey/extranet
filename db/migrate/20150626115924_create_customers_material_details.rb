class CreateCustomersMaterialDetails < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    create_table :customers_material_details do |t|
      t.integer :material_detail_id
      t.integer :customer_id

      t.timestamps
    end
    ActiveRecord::Base.establish_connection "development"

  end
end
