class AddTypeToAssistant < ActiveRecord::Migration
  def change
    add_column :assistants, :type, :string, default: 'EasternAssistant'
  end
end
