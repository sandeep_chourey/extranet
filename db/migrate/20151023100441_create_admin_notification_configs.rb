class CreateAdminNotificationConfigs < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    create_table :admin_notification_configs do |t|
      t.integer :activity_type_id
      t.string :notification_emails
      t.string :admin_region

      t.timestamps
    end
    ActiveRecord::Base.establish_connection "development"
  end
end
