class AddUserIdToSalesOrder < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    add_column :sales_orders, :user_id, :integer
    ActiveRecord::Base.establish_connection "production"
  end
end
