class CreateMaterialCodes < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def change
    create_table :material_codes do |t|
      t.integer :sales_order_id
      t.string :material_code

      t.timestamps
    end
    ActiveRecord::Base.establish_connection "production"
  end
end
