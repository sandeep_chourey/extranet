class RemoveCurrencyToMaterialDetail < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"
  def change
    remove_column :material_details, :currency, :string
    remove_column :material_details, :price, :integer
    ActiveRecord::Base.establish_connection "development"
  end
end
