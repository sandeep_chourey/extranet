class CreateSalesOrders < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"

  def up
    create_table :sales_orders do |t|
      t.string :sold_to_party, :limit => 10
      t.string :ship_to_party, :limit => 10
      t.date :required_delivery_date
      t.string :incoterms, :limit => 3
      t.string :po_number, :limit => 35
      t.date :po_date
      t.string :text
      t.string :status

      t.timestamps
    end
    ActiveRecord::Base.establish_connection "production"
  end


end
