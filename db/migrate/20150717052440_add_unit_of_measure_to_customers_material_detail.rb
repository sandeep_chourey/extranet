class AddUnitOfMeasureToCustomersMaterialDetail < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"
  def change
    add_column :customers_material_details, :unit_of_measure, :string
    ActiveRecord::Base.establish_connection "development"
  end
end
