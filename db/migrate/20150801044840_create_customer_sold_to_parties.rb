class CreateCustomerSoldToParties < ActiveRecord::Migration
  def change
    create_table :customer_sold_to_parties do |t|
      t.integer :customer_id
      t.string :sold_to

      t.timestamps
    end
  end
end
