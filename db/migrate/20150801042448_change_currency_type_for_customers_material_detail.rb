class ChangeCurrencyTypeForCustomersMaterialDetail < ActiveRecord::Migration
  ActiveRecord::Base.establish_connection "development_sec"
  def change
    change_column :customers_material_details, :price, :decimal, :precision => 10, :scale => 2, :default => 1.00
    ActiveRecord::Base.establish_connection "development"
  end
end
